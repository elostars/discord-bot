from discord import app_commands, Interaction
from discord.ext import commands

class Reload (commands.Cog):
    def __init__ (self, bot: commands.Bot) -> None:
        self.bot = bot

    @commands.Cog.listener ()
    async def on_ready (self) -> None:
        print (f'Cog "{self.qualified_name}" carregada')

    @commands.command (brief='Recarrega uma cog')
    @commands.is_owner ()
    async def reload (self, ctx, *, cog) -> None:
        await self.bot.reload_extension (f'cogs.{cog}')
        await ctx.send (f'Cog {cog} recarregada')
        
async def setup(bot: commands.Bot) -> None:
    await bot.add_cog (Reload (bot))
