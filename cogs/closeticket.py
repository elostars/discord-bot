import asyncio, os, discord
from discord import app_commands, Interaction, utils
from discord.ext import commands
from datetime import datetime

class CloseTicket (commands.Cog):
    def __init__ (self, bot: commands.Bot) -> None:
        self.bot = bot

    @commands.Cog.listener ()
    async def on_ready (self) -> None:
        print (f'Cog "{self.qualified_name}" carregada')

    @app_commands.command (description='Fecha o Ticket')
    async def closeticket(self, interaction: Interaction):
        if interaction.channel.name.startswith('ticket-') or interaction.channel.name.startswith('suporte-'):
            await interaction.response.send_message("O ticket será fechado em 1 minuto", ephemeral=True)
            with open(f"{interaction.channel.id}.md", 'a') as f:
                f.write(f"# Transcript of {interaction.channel.name}:\n\n")
                async for message in interaction.channel.history(limit = None, oldest_first = True):
                    created = datetime.strftime(message.created_at, "%d/%m/%Y at %H:%M:%S")
                    if message.edited_at:
                        edited = datetime.strftime(message.edited_at, "%d/%m/%Y at %H:%M:%S")
                        f.write(f"{message.author} on {created}: {message.clean_content} (Edited at {edited})\n\n")
                    else:
                        f.write(f"{message.author} on {created}: {message.clean_content}\n\n")
                generated = datetime.now().strftime("%d/%m/%Y at %H:%M:%S")
                f.write(f"\n*Generated at {generated} by {interaction.user.name}*\n*Date Formatting: DD/MM/YY*\n*Time Zone: UTC*")
            with open(f"{interaction.channel.id}.md", 'rb') as f:
                tcID = 1068295586467561493
                tChannel = interaction.guild.get_channel(tcID)
                await tChannel.send(file = discord.File(f, f"{interaction.channel.name}.md"))
            os.remove(f"{interaction.channel.id}.md")
            await asyncio.sleep(30)
            await interaction.channel.delete(reason="Ticket fechado")
        else:
            await interaction.response.send_message("Esse canal não é um ticket.", ephemeral=True)

async def setup (bot) -> None:
    await bot.add_cog (CloseTicket (bot))