import discord
from discord import app_commands, utils
from discord.ext import commands

class ticket_launcher(discord.ui.View):
    def __init__(self) -> None:
        super().__init__(timeout = None)


    @discord.ui.button(label = "Boosting", style = discord.ButtonStyle.blurple, custom_id = "boost_button")
    async def boost(self, interaction: discord.Interaction, button: discord.ui.Button):
        ticket = utils.get(interaction.guild.text_channels, name = f"ticket-{interaction.user.name}")
        if ticket: await interaction.response.send_message(f"Você já tem um ticket aberto {ticket.mention}!", ephemeral = True)
        else:
            attID = 1067684092075851829
            overwrites = {
            interaction.guild.default_role: discord.PermissionOverwrite(view_channel = False),
            interaction.user: discord.PermissionOverwrite(view_channel = True, send_messages = True, attach_files = True),
            interaction.guild.get_role(attID): discord.PermissionOverwrite(view_channel = True, send_messages = True, attach_files = True),
            }
            catID = 1061793751078543430
            category = interaction.guild.get_channel (catID)
            channel = await interaction.guild.create_text_channel(name = f"ticket-{interaction.user.name}", overwrites = overwrites, category = category)
            await channel.send(f"Bem vindo ao atendimento para Boosting {interaction.user.mention}!\n\nO <@&1067684092075851829> da EloStars respondera assim que possível.\n\nSe tiver criado o ticket por engano ou quiser fechar, é só digitar `/closeticket` !\n")
            await interaction.response.send_message(f"{channel.mention} foi criado!", ephemeral = True)

    @discord.ui.button(label = "DuoBoosting", style = discord.ButtonStyle.blurple, custom_id = "duo_button")
    async def duoboost(self, interaction: discord.Interaction, button: discord.ui.Button):
        ticket = utils.get(interaction.guild.text_channels, name = f"ticket-{interaction.user.name}")
        if ticket: await interaction.response.send_message(f"Você já tem um ticket aberto {ticket.mention}!", ephemeral = True)
        else:
            attID = 1067684092075851829
            overwrites = {
            interaction.guild.default_role: discord.PermissionOverwrite(view_channel = False),
            interaction.user: discord.PermissionOverwrite(view_channel = True, send_messages = True, attach_files = True),
            interaction.guild.get_role(attID): discord.PermissionOverwrite(view_channel = True, send_messages = True, attach_files = True),
            }
            catID = 1061793751078543430
            category = interaction.guild.get_channel (catID)
            channel = await interaction.guild.create_text_channel(name = f"ticket-{interaction.user.name}", overwrites = overwrites, category = category)
            await channel.send(f"Bem vindo ao atendimento para DuoBoosting {interaction.user.mention}!\n\nO <@&1067684092075851829> da EloStars respondera assim que possível.\n\nSe tiver criado o ticket por engano ou quiser fechar, é só digitar `/closeticket` !\n")
            await interaction.response.send_message(f"{channel.mention} foi criado!", ephemeral = True)

    @discord.ui.button(label = "Coaching", style = discord.ButtonStyle.blurple, custom_id = "coach_button")
    async def coach(self, interaction: discord.Interaction, button: discord.ui.Button):
        ticket = utils.get(interaction.guild.text_channels, name = f"ticket-{interaction.user.name}")
        if ticket: await interaction.response.send_message(f"Você já tem um ticket aberto {ticket.mention}!", ephemeral = True)
        else:
            attID = 1067684092075851829
            overwrites = {
            interaction.guild.default_role: discord.PermissionOverwrite(view_channel = False),
            interaction.user: discord.PermissionOverwrite(view_channel = True, send_messages = True, attach_files = True),
            interaction.guild.get_role(attID): discord.PermissionOverwrite(view_channel = True, send_messages = True, attach_files = True),
            }
            catID = 1067886873877418036
            category = interaction.guild.get_channel (catID)
            channel = await interaction.guild.create_text_channel(name = f"ticket-{interaction.user.name}", overwrites = overwrites, category = category)
            await channel.send(f"Bem vindo ao atendimento para Coaching {interaction.user.mention}!\n\nO <@&1067684092075851829> da EloStars respondera assim que possível.\n\nSe tiver criado o ticket por engano ou quiser fechar, é só digitar `/closeticket` !\n")
            await interaction.response.send_message(f"{channel.mention} foi criado!", ephemeral = True)

    @discord.ui.button(label = "MD10", style = discord.ButtonStyle.blurple, custom_id = "md10_button")
    async def md10(self, interaction: discord.Interaction, button: discord.ui.Button):
        ticket = utils.get(interaction.guild.text_channels, name = f"ticket-{interaction.user.name}")
        if ticket: await interaction.response.send_message(f"Você já tem um ticket aberto {ticket.mention}!", ephemeral = True)
        else:
            attID = 1067684092075851829
            overwrites = {
            interaction.guild.default_role: discord.PermissionOverwrite(view_channel = False),
            interaction.user: discord.PermissionOverwrite(view_channel = True, send_messages = True, attach_files = True),
            interaction.guild.get_role(attID): discord.PermissionOverwrite(view_channel = True, send_messages = True, attach_files = True),
            }
            catID = 1062662485171261450
            category = interaction.guild.get_channel (catID)
            channel = await interaction.guild.create_text_channel(name = f"ticket-{interaction.user.name}", overwrites = overwrites, category = category)
            await channel.send(f"Bem vindo ao atendimento para MD10 {interaction.user.mention}!\n\nO <@&1067684092075851829> da EloStars respondera assim que possível.\n\nSe tiver criado o ticket por engano ou quiser fechar, é só digitar `/closeticket` !\n")
            await interaction.response.send_message(f"{channel.mention} foi criado!", ephemeral = True)

class Services(commands.Cog):
    def __init__(self, bot: commands.Bot) -> None:
        self.bot = bot

    @commands.Cog.listener ()
    async def on_ready (self) -> None:
        print (f'Cog "{self.qualified_name}" carregada')
    @commands.has_role(1061751702505660467)
    @commands.command()
    async def services (self, interaction: discord.Interaction) -> None:
        msg = discord.Embed(title = "Bem vindo a EloStars!", color = 0x5f2892)
        msg.add_field(name="Selecione um serviço para continuar", value=" ", inline=False)
        msg.set_image(url='https://i.imgur.com/L7VVdd7.png')
        await interaction.channel.send(embed = msg, view = ticket_launcher())



async def setup(bot: commands.Bot) -> None:
    await bot.add_cog (Services (bot))