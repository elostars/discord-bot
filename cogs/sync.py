from discord import app_commands, Interaction
from discord.ext import commands

class Sync (commands.Cog):
    def __init__ (self, bot: commands.Bot) -> None:
        self.bot = bot

    @commands.Cog.listener ()
    async def on_ready (self) -> None:
        print (f'Cog "{self.qualified_name}" carregada')

    @commands.command ()
    @commands.is_owner ()
    async def sync (self, ctx) -> None:
        try:
            syncedCommands = await ctx.bot.tree.sync ()
        except Exception as exception:
            print (exception)
            return

        print ('Comandos sincronizados: ')
        cmds = len(syncedCommands)
        for i in range (cmds):
            print (f'{i}: {syncedCommands [i]}')
        
        await ctx.send (f'{cmds} {"comando sincronizado" if cmds == 1 else "comandos sincronizados"}')

async def setup (bot) -> None:
    await bot.add_cog (Sync (bot))
