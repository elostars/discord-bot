from discord import app_commands, Interaction
from discord.ext import commands

class Streaming (commands.Cog):
    def __init__ (self, bot: commands.Bot) -> None:
        self.bot = bot

    @commands.Cog.listener ()
    async def on_ready (self) -> None:
        print (f'Cog "{self.qualified_name}" carregada')
    @commands.has_role(1061751702505660467)
    @commands.command(brief = 'do nothing at all', hidden = True)
    async def streaming(self, ctx, streamer: str) -> None:
        if streamer == "pekaw":
            await ctx.send(f"https://www.twitch.tv/pekaaaw <@&1061758871485567099>")
        elif streamer == "pomps":
            await ctx.send(f"https://www.twitch.tv/biamendsz <@&1061758871485567099>")
        elif streamer == "stryx":
            await ctx.send(f"https://www.twitch.tv/stryxw <@&1061758871485567099>")
        else:
            await ctx.send("Diga-me um streamer!")

async def setup(bot: commands.Bot) -> None:
    await bot.add_cog (Streaming (bot))