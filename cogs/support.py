import discord
from discord import app_commands, utils
from discord.ext import commands

class ticket_launcher(discord.ui.View):
    def __init__(self) -> None:
        super().__init__(timeout = None)

    @discord.ui.button(label = "Abrir Ticket", style = discord.ButtonStyle.blurple, custom_id = "support_button")
    async def ticket(self, interaction: discord.Interaction, button: discord.ui.Button):
        ticket = utils.get(interaction.guild.text_channels, name = f"suporte-{interaction.user.name}")
        if ticket: await interaction.response.send_message(f"Você já tem um ticket aberto {ticket.mention}!", ephemeral = True)
        else:
            admID = 1070821182967922749
            overwrites = {
            interaction.guild.default_role: discord.PermissionOverwrite(view_channel = False),
            interaction.user: discord.PermissionOverwrite(view_channel = True, send_messages = True, attach_files = True),
            interaction.guild.get_role(admID): discord.PermissionOverwrite(view_channel = True, send_messages = True, attach_files = True),
            }
            catID = 1064469891505209404
            category = interaction.guild.get_channel (catID)
            channel = await interaction.guild.create_text_channel(name = f"suporte-{interaction.user.name}", overwrites = overwrites, category = category)
            await channel.send(f"Olá {interaction.user.mention}!\n\nUm <@&1070821182967922749> da EloStars respondera assim que possível.\n\nSe tiver criado o ticket por engano ou quiser fechar, é só digitar `/closeticket` !")
            await interaction.response.send_message(f"{channel.mention} foi criado!", ephemeral = True)

class Support(commands.Cog):
    def __init__(self, bot: commands.Bot) -> None:
        self.bot = bot

    @commands.Cog.listener ()
    async def on_ready (self) -> None:
        print (f'Cog "{self.qualified_name}" carregada')
        
    @commands.has_role(1061751702505660467)
    @commands.command()
    async def support (self, interaction: discord.Interaction) -> None:
        msg = discord.Embed(title = "Bem vindo a EloStars!", color = 0x5f2892)
        msg.add_field(name="Abra um ticket no suporte!", value=" ", inline=False)
        msg.set_image(url='https://i.imgur.com/L7VVdd7.png')
        await interaction.channel.send(embed = msg, view = ticket_launcher())



async def setup(bot: commands.Bot) -> None:
    await bot.add_cog (Support (bot))