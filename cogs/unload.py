from discord import app_commands, Interaction
from discord.ext import commands

class Unload (commands.Cog):
    def __init__ (self, bot: commands.Bot) -> None:
        self.bot = bot

    @commands.Cog.listener ()
    async def on_ready (self):
        print (f'Cog "{self.qualified_name}" carregada')

    @commands.command (brief='Descarrega uma cog')
    @commands.is_owner ()
    async def unload (self, ctx, *, cog) -> None:
        await self.bot.unload_extension (f'cogs.{cog}')
        await ctx.send (f'Cog {cog} descarregada')
        
async def setup (bot: commands.Bot) -> None:
    await bot.add_cog (Unload (bot))