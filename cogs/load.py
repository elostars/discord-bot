from discord import app_commands, Interaction
from discord.ext import commands

class Load (commands.Cog):
    def __init__ (self, bot: commands.Bot) -> None:
        self.bot = bot

    @commands.Cog.listener ()
    async def on_ready (self) -> None:
        print (f'Cog "{self.qualified_name}" carregada')

    @commands.command (brief = 'Carrega uma cog', hidden = True)
    @commands.is_owner ()
    async def load (self, ctx, *, cog) -> None:
        await self.bot.load_extension (f'cogs.{cog}')
        await ctx.send (f'Cog {cog} carregada')
        
async def setup(bot: commands.Bot) -> None:
    await bot.add_cog (Load (bot))
