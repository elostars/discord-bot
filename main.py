from discord import Intents, Object, utils
from asyncio import run
from os import listdir, environ
from discord.ext import commands

intents = Intents.all ()
intents.message_content = True
bot = commands.Bot (command_prefix = '!', intents = intents, application_id = '1062879359104450730')
bot.remove_command ('help')

async def load ():
    for file in listdir ('./cogs'):
        if file.endswith ('.py'):
            await bot.load_extension (f'cogs.{file[:-3]}')

async def main():
    await load()
    utils.setup_logging()
    await bot.start (environ.get ('DISCORD_API_KEY'))

run (main())